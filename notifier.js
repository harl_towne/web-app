const { response } = require('express')
const { Client } = require('pg')
const webpush = require("web-push")
require('dotenv').config()

const publicVapidKey = "BCVEjTKYA8r8nAghfV8iAaLqzveSPR2oMIuUYw3eCSk8iemQZazJ_ghm3iCBJWhzwfvibUnX7GdQDGEItWUYINE"//process.env.PUBLIC_VAPID_KEY;
const privateVapidKey = "BTH0XwIXqGNaYMsm-RllU3oOyb13LINdEqupupZZRYQ"//process.env.PRIVATE_VAPID_KEY;

//hours before each event to remind user - data is structured in the same way as the checklist data
//dates without times are assumed to be at 12am
const REMINDER_TIMES = {
  "mainPage":
   {
    "surgeryDate": 12
  },
  "myAppointmentsBeforeMySurgery":
  {
    "appointmentWithMyDoctor":24,
    "preadmissionClinicAppointment":24,
    "anaestheticsReview":24,
    "scansOrTests":24
  },
  "23weeksBeforeMySurgery":
  {
    "bloodTest":24
  },
  "theNightBeforeAndMorningOfMySurgery":
  {
    "timeOfArrival":24,
    "timeToStopEating":24,
    "timeToStopDrinking":24
  },
  "instructionsForMyMedicationBeforeMySurgery": 16,
  "thingsINeedToKnowBeforeIGoHome":
  {
    "myFollowUpAppointment": 24,
    "myWoundDressings": 12,
    "myActivityLevels": 
    {
      "goBackToWork": 12,
      "startDriving": 12,
      "startExercising": 12,
      "liftHeavierObjects": 12
    }
  }
}

console.log("notifier running")

//get database url and connect to it
console.log("database is: " + process.env.DATABASE_URL)
  const client = new Client({
  connectionString: process.env.DATABASE_URL
})
client.connect()

//setup push notifications
webpush.setVapidDetails('mailto:harl.t@hotmail.com.au', publicVapidKey, privateVapidKey);

//query the database for all users that have a checklist to send notifications about and a subscription to send the notifications to
const DBreq = client.query("SELECT name, urn, checklist, subscription FROM users WHERE checklist::text != '{}'::text AND subscription::text != '{}'::text;")
.then(async (response) => {
  //get the current date and time
  const current_datetime = Date.now()
  //send notifications for each user
  for(row in response.rows) //each row is the entry for a user
  {
    //store each piece of data from the query in a variable
    const name = response.rows[row]["name"]
    const urn = response.rows[row]["urn"]
    var checklist = response.rows[row]["checklist"]
    const subscription = response.rows[row]["subscription"]

    //send notifications for each checklist section
    for(key1 in checklist)//each top level key is a checklist section
    {
      //check which section is being checked for notifications
      switch(key1)
      {
        //main checklist page (only contains surgery date)
        case "mainPage":
          //check if there is an entry for the surgery date
          if(checklist[key1].hasOwnProperty("surgeryDate"))
          {
            //store whether the notification for the event in a boolean variable - default to false
            var notification_sent = false
            if(checklist[key1]["surgeryDate"].hasOwnProperty("notificationSent"))
            {
              notification_sent = checklist[key1]["surgeryDate"]["notificationSent"]
            }
            //convert the date string to datetime (time assumed to be 12am)
            var surgery_date = Date.parse(checklist[key1]["surgeryDate"]["date"] + " UTC+10:00")
            //get the hours between the current time and the event (negative for past events)
            var diff_hours = (surgery_date-current_datetime)/1000/60/60
            //check if there is less time until the event than the REMINDER_TIMES specifies and if the notification hasn't already been sent
            if(diff_hours < REMINDER_TIMES[key1]["surgeryDate"] && !notification_sent)
            {
              // send notification
              sendNotification(`Surgery Reminder for ${name}`, "Your surgery is tomorrow.", subscription)
              // sent notification sent status to prevent repeated notifications
              checklist[key1]["surgeryDate"]["notificationSent"] = true
            }
          }
          break

        // "My Appointments Before Surgery" section - all notification events have time and date
        case "myAppointmentsBeforeMySurgery":
          // each second level key is a appointment
          for(key2 in checklist[key1])
          {
            //store whether the notification for the event in a boolean variable - default to false
            var notification_sent = false
            if(checklist[key1][key2].hasOwnProperty("notificationSent"))
            {
              notification_sent = checklist[key1][key2]["notificationSent"]
            }
            //convert the date string to datetime
            var datetime = Date.parse(checklist[key1][key2]["date"] + " " + checklist[key1][key2]["time"] + " UTC+10:00")
            //get the hours between the current time and the event (negative for past events)
            var diff_hours = (datetime-current_datetime)/1000/60/60
            //check if there is less time until the event than the REMINDER_TIMES specifies and if the notification hasn't already been sent
            if(diff_hours < REMINDER_TIMES[key1][key2] && !notification_sent)
            {
              // this object stores the text variations for the notifiation
              var dict = {
                "appointmentWithMyDoctor": "appointment with your doctor",
                "preadmissionClinicAppointment": "preadmission clinic appointment",
                "anaestheticsReview": "anaesthetics review",
                "scansOrTests": "scan or test"
              }
              // send notification
              sendNotification(`Apointment Reminder for ${name}`, `You have a ${dict[key2]} tomorrow.`, subscription)
              // sent notification sent status to prevent repeated notifications
              checklist[key1][key2]["notificationSent"] = true
            }
          }
          break

        // "2 – 3 Weeks Before My Surgery" section - the only notification is for the blood test appointment
        case "23weeksBeforeMySurgery":
          // the only appointment in this section is the blood test appointment
          var key2 = "bloodTest"
          //check if the blood test appointment exists
          if(checklist[key1].hasOwnProperty(key2)) 
          {
            //store whether the notification for the event in a boolean variable - default to false
            var notification_sent = false
            if(checklist[key1][key2].hasOwnProperty("notificationSent"))
            {
              notification_sent = checklist[key1][key2]["notificationSent"]
            }
            //convert the date string to datetime
            var datetime = Date.parse(checklist[key1][key2]["date"] + " " + checklist[key1][key2]["time"] + " UTC+10:00")
            //get the hours between the current time and the event (negative for past events)
            var diff_hours = (datetime-current_datetime)/1000/60/60
            //check if there is less time until the event than the REMINDER_TIMES specifies and if the notification hasn't already been sent
            if(diff_hours < REMINDER_TIMES[key1][key2] && !notification_sent)
            {
              // send notification
              sendNotification(`Apointment Reminder for ${name}`, "You have a blood test tomorrow.", subscription)
              // sent notification sent status to prevent repeated notifications
              checklist[key1][key2]["notificationSent"] = true
            }
          }
          break

        // "The Night Before & Morning of My Surgery"  - notifications for some events shortly before the surgery date
        case "theNightBeforeAndMorningOfMySurgery":
          // check if the user has entered the date and time-of-arrival for the surgery - if not the times in this section can't be translated to specific dates and times
          if(checklist.hasOwnProperty("mainPage") && checklist["mainPage"].hasOwnProperty("surgeryDate") && checklist[key1].hasOwnProperty("timeOfArrival"))
          {
            //get the difference in hours between now and the time of arrival
            var surgery_date = checklist["mainPage"]["surgeryDate"]["date"]
            var surgeryTOA_datetime = Date.parse(surgery_date + " " + checklist[key1]["timeOfArrival"]["time"] + " UTC+10:00")
            var surgeryTOA_diff_hours = (surgeryTOA_datetime - current_datetime)/1000/60/60
            //for each event in this section
            for(key2 in checklist[key1])
            {
              //store whether the notification for the event in a boolean variable - default to false
              var notification_sent = false
              if(checklist[key1][key2].hasOwnProperty("notificationSent"))
              {
                notification_sent = checklist[key1][key2]["notificationSent"]
              }
              //convert the date string to datetime
              var datetime = Date.parse(surgery_date + " " + checklist[key1][key2]["time"] + " UTC+10:00")
              //get the hours between the current time and the event (negative for past events)
              var diff_hours = (datetime-current_datetime)/1000/60/60
              //check if the current diff_hours puts the time and date of the event after the time of arrival for the surgery
              while(surgeryTOA_diff_hours < diff_hours)
              {
                //if so bring the time back 24 hours
                diff_hours -= 24
              }
              //check if there is less time until the event than the REMINDER_TIMES specifies and if the notification hasn't already been sent
              if(diff_hours < REMINDER_TIMES[key1][key2] && !notification_sent)
              {
                // this object stores the text variations for the notifiation
                var dict = 
                {
                  "timeOfArrival": "arrive at the hostpital",
                  "timeToStopEating": "stop eating",
                  "timeToStopDrinking": "stop drinking"
                }
                // send notification
                sendNotification(`Reminder for ${name}`, `You need to ${dict[key2]} in ${REMINDER_TIMES[key1][key2]} hours.`, subscription)
                // sent notification sent status to prevent repeated notifications
                checklist[key1][key2]["notificationSent"] = true
              }
            }
          }
          break

        // there are no norifications for the "If I am Having a Day Case Procedure" section
        case "ifIAmHavingADayCaseProcedure":
          break

        // "Instructions for My Medication Before Surgery" section - one notification per medication entered
        case "instructionsForMyMedicationBeforeMySurgery":
          // check if the user has entered the date for the surgery - if not the events in this section can't be translated to specific dates and times
          if(checklist.hasOwnProperty("mainPage") && checklist["mainPage"].hasOwnProperty("surgeryDate"))
          {
            //find the diff_hours between now and the 12:01am of the surgery date
            var surgery_date = checklist["mainPage"]["surgeryDate"]["date"]
            var surgery_datetime = Date.parse(surgery_date + " UTC+10:00")
            var base_diff_hours = (surgery_datetime - current_datetime)/1000/60/60
            //each second level key is the index for a medication which the user endered
            for(key2 in checklist[key1])
            {
              // check if the medication entry is null or if the "when to stop" entry is not a number
              // the entry is null if the user has deleted the entry
              if(checklist[key1][key2] == null || isNaN(parseInt(checklist[key1][key2]["whenToStop"])))
              {
                continue
              }
              //get the number of days before the surgery the medication is to be stopped
              var days = parseInt(checklist[key1][key2]["whenToStop"])
              //store whether the notification for the event in a boolean variable - default to false
              var notification_sent = false
              if(checklist[key1][key2].hasOwnProperty("notificationSent"))
              {
                notification_sent = checklist[key1][key2]["notificationSent"]
              }
              //work out the diff_hours using the diff_hours for the surgery date and the days before the surgery to stop the medication
              var diff_hours = base_diff_hours - 24 * (days-1)
              //check if there is less time until the event than the REMINDER_TIMES specifies and if the notification hasn't already been sent
              if(diff_hours < REMINDER_TIMES[key1] && !notification_sent)
              {
                // send notification
                sendNotification(`Medication Reminder for ${name}`, `You will have to stop taking ${checklist[key1][key2]["name"]} tomorrow.`, subscription)
                // sent notification sent status to prevent repeated notifications
                checklist[key1][key2]["notificationSent"] = true
              }
            }
          }
          break

        // "Things I need to know before I go home" section - various events after the surgery date
        case "thingsINeedToKnowBeforeIGoHome":
          //each second level key is an event
          for(key2 in checklist[key1])
          {
            //check which event it is
            switch(key2)
            {
              // event is a follow up appointment
              case "myFollowUpAppointment":
                //store whether the notification for the event in a boolean variable - default to false
                var notification_sent = false
                if(checklist[key1][key2].hasOwnProperty("notificationSent"))
                {
                  notification_sent = checklist[key1][key2]["notificationSent"]
                }
                //convert the date string to datetime
                var datetime = Date.parse(checklist[key1][key2]["date"] + " " + checklist[key1][key2]["time"] + " UTC+10:00")
                //get the hours between the current time and the event (negative for past events)
                var diff_hours = (datetime-current_datetime)/1000/60/60
                //check if there is less time until the event than the REMINDER_TIMES specifies and if the notification hasn't already been sent
                if(diff_hours < REMINDER_TIMES[key1][key2] && !notification_sent)
                {
                  // send notification
                  sendNotification(`Appointment Reminder for ${name}`, `You have a followup appointment tomorrow with ${checklist[key1][key2]["doctor"]} at ${checklist[key1][key2]["location"]}.`, subscription)
                  // sent notification sent status to prevent repeated notifications
                  checklist[key1][key2]["notificationSent"] = true
                }
                break

              //event for when the user can take off their wound dressings
              case "myWoundDressings":
                // check if the user has entered the date for the surgery - if not this event can't be translated to a specific date
                if(checklist.hasOwnProperty("mainPage") && checklist["mainPage"].hasOwnProperty("surgeryDate"))
                {
                  //find the diff_hours between now and the 12:01am of the surgery date
                  var surgery_date = checklist["mainPage"]["surgeryDate"]["date"]
                  var surgery_datetime = Date.parse(surgery_date + " UTC+10:00")
                  var base_diff_hours = (surgery_datetime - current_datetime)/1000/60/60
                  //check if the "leave for days" entry is null or not a number, if so the skip this event
                  if(checklist[key1][key2]["leaveForDays"] == null || isNaN(parseInt(checklist[key1][key2]["leaveForDays"])))
                  {
                    continue
                  }
                  //store whether the notification for the event in a boolean variable - default to false
                  var notification_sent = false
                  if(checklist[key1][key2].hasOwnProperty("notificationSent"))
                  {
                    notification_sent = checklist[key1][key2]["notificationSent"]
                  }
                  //get the number of days after the surgery the wound dressings can be removed
                  var days = parseInt(checklist[key1][key2]["leaveForDays"])
                  //work out the diff_hours using the diff_hours for the surgery date and the days after the surgery the wound dressings can be removed
                  var diff_hours = base_diff_hours + 24 * (days)
                  //check if there is less time until the event than the REMINDER_TIMES specifies and if the notification hasn't already been sent
                  if(diff_hours < REMINDER_TIMES[key1][key2] && !notification_sent)
                  {
                    // send notification
                    sendNotification(`Reminder for ${name}`, "You can take off your wound dressings tomorrow", subscription)
                    // sent notification sent status to prevent repeated notifications
                    checklist[key1][key2]["notificationSent"] = true
                  }
                }
                break

              //there are no notifications for the "my diet" entry
              case "myDiet":
                break

              //set of events for when the user can resume certain activities
              case "myActivityLevels":
                //each third level key is an activity
                for(key3 in checklist[key1][key2])
                {
                  //check if the entry for the activity is null - if so skip this activity
                  //entry is null when user enters info for the other actifvities but no this one
                  if(checklist[key1][key2][key3] == null)
                  {
                    continue
                  }
                  //convert the date string to datetime (time assumed to be 12am)
                  var datetime = Date.parse(checklist[key1][key2][key3]["date"] + " UTC+10:00")
                  //get the hours between the current time and the event (negative for past events)
                  var diff_hours = (datetime-current_datetime)/1000/60/60
                  //check if there is less time until the event than the REMINDER_TIMES specifies and if the notification hasn't already been sent
                  if(diff_hours < REMINDER_TIMES[key1][key2][key3] && !notification_sent)
                  {
                    // this object stores the text variations for the notifiation
                    var dict = {
                      "goBackToWork": "go back to work",
                      "startDriving": "start driving",
                      "startExercising": "start exersising",
                      "liftHeavierObjects": "lift heavy objects"
                    }
                    // send notification 
                    sendNotification(`Activity Reminder for ${name}`, `You can ${dict[key3]} again tomorrow`, subscription)
                    // sent notification sent status to prevent repeated notifications
                    checklist[key1][key2][key3]["notificationSent"] = true
                  }
                }
                break
            }
          }
          break

        //if the key did not match any of the sections print a warning to the console
        default:
          console.warn(`Unknown key: ${key1}`)
      }
    }
    console.log("#######################################################")
    // after sending all relevant notifications and updating checklist with new "notificationSent" values:
    // query the database to update the users checklist entry
    const DBupdate = await client.query(`UPDATE users SET checklist = '${JSON.stringify(checklist)}' WHERE urn='${urn}';`)
    .then((response) => {
      console.log(`Updated checklist for ${name} (${urn})`)
    })
    .catch((reason) =>{
      console.error(reason)
    })
  }
  //after all users have been checked through
  console.log("notifier finished")
  //end the connection to the database
  client.end()
})
.catch((reason) => {
  //if there was an error getting the users print the error
  console.error(reason)
  //then end the connection to the database
  client.end()
})

/**
 * Function takes in title and body of a push notification along with the subscription object of the device to send it to.
 * Function then sends a notification with this info and a default icon
 * 
 * @param {String} title title of the notification
 * @param {String} body body of the notification
 * @param {JSON} subscription subscription of the device to send the notification to
 */
function sendNotification(title, body, subscription)
{
  //create the payload of the notification
  var payload = JSON.stringify({"title": title, other: {body: body, icon: "https://my-rbwh-surgery-pathway.herokuapp.com/icons/notification.png"}})
  console.log("=======================================================")
  console.log(title)
  console.log(body)
  //send the notification payload using the subscription
  webpush.sendNotification(subscription, payload)
  .catch((reason) =>{
    //if there was an error print it to console
    console.error(reason)
  })
}