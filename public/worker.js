console.log("worker started")

//when a push notification is received
self.addEventListener('push', ev => 
{
  //get the notification data
  const data = ev.data.json();
  //show the notifications
  self.registration.showNotification(data.title, data.other);
});