//key used for web notifications
const publicVapidKey = 'BCVEjTKYA8r8nAghfV8iAaLqzveSPR2oMIuUYw3eCSk8iemQZazJ_ghm3iCBJWhzwfvibUnX7GdQDGEItWUYINE'

/**
 * displays the users current checklists
 */
function displayChecklists()
{
  //if the user hasn't logged in display the login page
  if(URN == null || URN == "" || PASSWORD == null || PASSWORD == "")
  {
    userButtonCLick() 
    return
  }

  //scroll to top of page
  window.scrollTo(0,0)
  //send request for checklists page
  var response = synchronousLoad("/checklists.html")
  //if the request is successful paste the content to the page
  var content = ""
  if(response != null)
  {
    content = response
  }
  //otherwise paste an error message
  else
  {
    content = "failed to get page"
  }

  //create request for checklist data
  var checklistData_req = new XMLHttpRequest()
  var data = JSON.stringify({"urn": URN, "password": PASSWORD});
  var url = domain
  url += "/json/getchecklist"
  checklistData_req.open("POST", url, false)
  checklistData_req.setRequestHeader("Content-Type", "application/json")
  checklistData_req.send(data)
  //if the request was successful parse the data and store it for later 
  if(checklistData_req.status == 200)
  {
    CHECKLIST_DATA = JSON.parse(checklistData_req.responseText)
  }
  //if it was unsuccessful replace the content with an error message
  else 
  {
    content = "failed to get checklist data, error: " + checklistData_req.status
  }
  document.getElementById("content").innerHTML = content;

  //fill surger date cell with data
  surgery_data_cell = document.getElementById("surgeryDate")
  displayChecklistData(surgery_data_cell, "mainPage->surgeryDate", "edit-delete")
}

/**
 * function for getting the specific checklist sections
 * 
 * @param {Object} checklistButton the div object that was clicked on to call this function, the id 
 *                      of the div is used to decide which section to get
 */
function checklistSectionClick(checklistButton)
{
  //hide title bar and article sections, show article header, adjust headerSpacer to fit
  document.getElementById("navbar").style.visibility = "hidden"
  document.getElementById("titlebar").style.visibility = "hidden"
  document.getElementById("articleHeader").style.visibility = "visible"
  document.getElementById("searchBarHeader").style.visibility = "hidden"  
  document.getElementById("headerSpacer").style.paddingBottom = "12.5%"
  
  //get section
  checklistSection = checklistButton.id
  current_checklist_section = checklistSection
  //scroll to top of page
  window.scrollTo(0,0)

  //send request for page
  var response = synchronousLoad("/checklistSections/" + checklistSection + ".html")

  //if the request is successful
  var content = ""
  if(response != null)
  {
    //set the content to the response to the request
    content = response

    //paste the response into the page
    document.getElementById("content").innerHTML = content;
    //check if the section is the "instructions for my medications before my surgery" section and check if there is any data for that section
    if(checklistSection == "instructionsForMyMedicationBeforeMySurgery" && CHECKLIST_DATA.hasOwnProperty("instructionsForMyMedicationBeforeMySurgery"))
    {
      //get the table that the data is diaplyed in
      var table = document.getElementsByTagName("table")[0]
      //for each medication listing
      for(var i = 0; i < CHECKLIST_DATA["instructionsForMyMedicationBeforeMySurgery"].length; i++)
      {
        //check if the listing is empty (deleted lisings are set to null)
        if(CHECKLIST_DATA["instructionsForMyMedicationBeforeMySurgery"][i] == null || CHECKLIST_DATA["instructionsForMyMedicationBeforeMySurgery"][i] == "")
        {
          //if so skip the rest of this loop
          continue
        }
        //create new row for the medication listing
        var row = table.insertRow(table.rows.length - 1)
        //create two cells in the row
        var leftCell = row.insertCell(0)
        var rightCell = row.insertCell(1)
        //set the class names so that the next for loop can find them
        leftCell.className =  "checklistSectionData"
        rightCell.className =  "checklistSectionData"
        //set the id's so that the next for loop can identify them and put the correct data in each (id contains index for the correct piece of data)
        leftCell.id = i + "->name"
        rightCell.id = i + "->whenToStop"
      }
    }

    //get all the table cells that need to have data inserted into them 
    var temp_cells = document.getElementsByClassName("checklistSectionData")
    for(var i = temp_cells.length - 1; i >=0; i--)
    { 
      //if the cell has an id and if the cells id does not end in "->name"
      if(temp_cells[i].id != null && temp_cells[i].id != "" && temp_cells[i].id.split("->")[1] != "name")
      {
        //insert data into this cell
        displayChecklistData(temp_cells[i], checklistSection + "->" + temp_cells[i].id, "edit-delete")
        /*
          data is not inserted into cells with and id that ends in "->name" because data is automaticcaly added 
          to them with data is inserted into the cell next to them (which ends it "->whenToStop"). this only applys
          to the "instructions for my medications before my surgery" page
        */
      }
    }
  }
  //if the request is unsuccessful paste an error message
  else
  {
    content = "failed to get page"
    document.getElementById("content").innerHTML = content;
  }
}
/**
 * function for returning to the main checklist page
 */
 
function returntoChecklist()
{
  //hide title bar and article sections, show article header, adjust headerSpacer to fit
  document.getElementById("navbar").style.visibility = "hidden"
  document.getElementById("titlebar").style.visibility = "hidden"
  document.getElementById("articleHeader").style.visibility = "visible"
  document.getElementById("searchBarHeader").style.visibility = "hidden"  
  document.getElementById("headerSpacer").style.paddingBottom = "12.5%"
  
  //scroll to top of page
  window.scrollTo(0,0)
  checklistSection = current_checklist_section
  //send request for page
  var response = synchronousLoad("/checklistSections/" + checklistSection + ".html")

  //if the request is successful
  var content = ""
  if(response != null)
  {
    //set the content to the response to the request
    content = response

    //paste the response into the page
    document.getElementById("content").innerHTML = content;
    //check if the section is the "instructions for my medications before my surgery" section and check if there is any data for that section
    if(checklistSection == "instructionsForMyMedicationBeforeMySurgery" && CHECKLIST_DATA.hasOwnProperty("instructionsForMyMedicationBeforeMySurgery"))
    {
      //get the table that the data is diaplyed in
      var table = document.getElementsByTagName("table")[0]
      //for each medication listing
      for(var i = 0; i < CHECKLIST_DATA["instructionsForMyMedicationBeforeMySurgery"].length; i++)
      {
        //check if the listing is empty (deleted lisings are set to null)
        if(CHECKLIST_DATA["instructionsForMyMedicationBeforeMySurgery"][i] == null || CHECKLIST_DATA["instructionsForMyMedicationBeforeMySurgery"][i] == "")
        {
          //if so skip the rest of this loop
          continue
        }
        //create new row for the medication listing
        var row = table.insertRow(table.rows.length - 1)
        //create two cells in the row
        var leftCell = row.insertCell(0)
        var rightCell = row.insertCell(1)
        //set the class names so that the next for loop can find them
        leftCell.className =  "checklistSectionData"
        rightCell.className =  "checklistSectionData"
        //set the id's so that the next for loop can identify them and put the correct data in each (id contains index for the correct piece of data)
        leftCell.id = i + "->name"
        rightCell.id = i + "->whenToStop"
      }
    }

    //get all the table cells that need to have data inserted into them 
    var temp_cells = document.getElementsByClassName("checklistSectionData")
    for(var i = temp_cells.length - 1; i >=0; i--)
    { 
      //if the cell has an id and if the cells id does not end in "->name"
      if(temp_cells[i].id != null && temp_cells[i].id != "" && temp_cells[i].id != temp_cells[i].id[0] + "->name")
      {
        //insert data into this cell
        displayChecklistData(temp_cells[i], checklistSection + "->" + temp_cells[i].id, "edit-delete")
        /*
          data is not inserted into cells with and id that ends in "->name" because data is automaticcaly added 
          to them with data is inserted into the cell next to them (which ends it "->whenToStop"). this only applys
          to the "instructions for my medications before my surgery" page
        */
      }
    }
  }
  //if the request is unsuccessful paste an error message
  else
  {
    content = "failed to get page"
    document.getElementById("content").innerHTML = content;
  }
}
/**
 * Function displays data in the HTML object given by the varibale "dataCell". The data is found using the "dataLocation variable". If no data is available
 * and add button will be added to the checklist to allow the user to insert data, except if the section is the "instructions for my medications before my surgery" 
 * section (found using the first index from "dataLocation") in which case the row will be deleted. Button set is used to specify which buttons should be in the
 * HTML Object
 * 
 * @param {HTML Object} dataCell HTML object that the data is being inserted into
 * 
 * @param {String} dataLocation Location of the data in the CHECKLIST_DATA JSON Object. format: "{index1}->{index2}->{index3}->etc"
 * 
 * @param {String} buttonSet  Button set to include in the HTML object. Options: "edit-delete", "yes-no" or "". the "delete" button 
 *                            will recall this function to display the data with the "yes-no" buttons, the "yes" button will delete 
 *                            that data and the "no" button will recall this function with the "edit-delete" buttons to return to the original state.
 */
function displayChecklistData(dataCell, dataLocation, buttonSet)
{
  //split the data location into an array
  var dataLocation_split = dataLocation.split("->")
  var content = "" //content variable to eventualy be pasted into dataCell
  //check if data exists for this dataCell
  //if it does begin pasting into the content variable
  if(CHECKLIST_DATA.hasOwnProperty(dataLocation_split[0]) && CHECKLIST_DATA[dataLocation_split[0]].hasOwnProperty(dataLocation_split[1]))
  {
    //check which section the data is from
    switch(dataLocation_split[0])
    {
      //main section (only contains surgery date)
      case "mainPage":
        //add field for date only
        //for date of surgery
        content += `
        Date: ${reformatDate(CHECKLIST_DATA[dataLocation_split[0]][dataLocation_split[1]]["date"])}
        <br>`      
        break

      //"The Night Before & Morning of Surgery" section
      case "theNightBeforeAndMorningOfMySurgery":
        //add field for time only
        //for time to arrive/stop eating/stop drinking
        content += `
        Time: ${reformatTime(CHECKLIST_DATA[dataLocation_split[0]][dataLocation_split[1]]["time"])}
        <br>`      
        break

      //"If I am Having a Day Case Procedure (Discharge same day as surgery)" section
      case "ifIAmHavingADayCaseProcedure":
        //add fields for name, relationship and phone number
        //for contact details of support person
        content += `
        Name: ${CHECKLIST_DATA[dataLocation_split[0]][dataLocation_split[1]]["name"]} 
        <br>
        Relationship to you: ${CHECKLIST_DATA[dataLocation_split[0]][dataLocation_split[1]]["relationship"]}
        <br>
        Phone number: ${CHECKLIST_DATA[dataLocation_split[0]][dataLocation_split[1]]["phonenumber"]}
        <br>`
        break

      //"Things I need to know before I go home" section
      case "thingsINeedToKnowBeforeIGoHome":
        //check which specific cell is having data added to it (each is different)
        switch(dataLocation_split[1])
        {
          //"My follow up appointment" dataCell
          case "myFollowUpAppointment":
            //add data, time, doctor and location fields
            //details for follow appointment
            content += `
            Date: ${reformatDate(CHECKLIST_DATA[dataLocation_split[0]][dataLocation_split[1]]["date"])} 
            <br>
            Time: ${reformatTime(CHECKLIST_DATA[dataLocation_split[0]][dataLocation_split[1]]["time"])}
            <br>
            Doctor: ${CHECKLIST_DATA[dataLocation_split[0]][dataLocation_split[1]]["doctor"]}
            <br>
            Location: ${CHECKLIST_DATA[dataLocation_split[0]][dataLocation_split[1]]["location"]}
            <br>`
            break

          //"My wound dressings" dataCell
          case "myWoundDressings":
            //add fields for days to leave dressings and other details
            //for info on wound dressings
            content += `
            Leave wound dressing intact for ${CHECKLIST_DATA[dataLocation_split[0]][dataLocation_split[1]]["leaveForDays"]} days.
            <br>
            Other: ${CHECKLIST_DATA[dataLocation_split[0]][dataLocation_split[1]]["other"]}
            <br>`
            break

          //"My diet" dataCell
          case "myDiet":
            //add fields for type of diet and other info
            //for info on diet after surgery
            content += `
            Type ${CHECKLIST_DATA[dataLocation_split[0]][dataLocation_split[1]]["type"]}
            <br>
            Other: ${CHECKLIST_DATA[dataLocation_split[0]][dataLocation_split[1]]["other"]}
            <br>`
            break

          //"My activity levels" dataCell
          case "myActivityLevels":
            //add fields for work, driving, exersing and lifting
            //for into on when certain activities can be resumed after surgery
            content += `
            When I can:
            <ul>
                <li>
                    Go back to work: ${CHECKLIST_DATA[dataLocation_split[0]][dataLocation_split[1]]["goBackToWork"]["date"]}
                </li>
                <li>
                    Start driving: ${CHECKLIST_DATA[dataLocation_split[0]][dataLocation_split[1]]["startDriving"]["date"]}
                </li>
                <li>
                    Start exercising: ${CHECKLIST_DATA[dataLocation_split[0]][dataLocation_split[1]]["startExercising"]["date"]}
                </li>
                <li>
                    Life heavier objects: ${CHECKLIST_DATA[dataLocation_split[0]][dataLocation_split[1]]["liftHeavierObjects"]["date"]}
                </li>
            </ul>`
            break
        }
        break

      //"Instructions for My Medication Before Surgery" section
      case "instructionsForMyMedicationBeforeMySurgery":
        //get coresponding cell to the left of this one and add the name of medication to it
        document.getElementById(`${dataLocation_split[1]}->name`).innerHTML = CHECKLIST_DATA[dataLocation_split[0]][dataLocation_split[1]]["name"]
        //add when to stop the medication to this cell
        content += `${CHECKLIST_DATA[dataLocation_split[0]][dataLocation_split[1]]["whenToStop"]} days before surgery`
        break

      //"My Appointments Before Surgery" and "2 – 3 Weeks Before My Surgery" sections
      default:
        //add date time and location fields
        //used for multiple appointments
        content += `
        Date: ${reformatDate(CHECKLIST_DATA[dataLocation_split[0]][dataLocation_split[1]]["date"])} 
        <br>
        Time: ${reformatTime(CHECKLIST_DATA[dataLocation_split[0]][dataLocation_split[1]]["time"])}
        <br>
        Location: ${CHECKLIST_DATA[dataLocation_split[0]][dataLocation_split[1]]["location"]}
        <br>`
        break
    }
    //check which button set should be put into the dataCell
    switch(buttonSet)
    {
      //edit and delete button set
      case "edit-delete":
        //edit button calls the addInputs() function
        //delete button changes buttons to the yes-no button set
        content += `
        <button onclick="addInputs(this.parentNode, '${dataLocation}')">Edit</button>
        <button onclick="displayChecklistData(this.parentNode, '${dataLocation}', 'yes-no')">Delete</Button>`
        break

      //yes and no button set
      case "yes-no":
        //yes button deletes the data but calling the updateChecklistData() then calls this function again to replace the data with an add button or delete the row (depending on checklist section)
        //the no button calls this functon to go back to the edit-delete button set
        content += `
        <button onclick="updateChecklistData('${dataLocation}', null);displayChecklistData(this.parentNode, '${dataLocation}', 'edit-delete')">Yes</button>
        <button onclick="displayChecklistData(this.parentNode, '${dataLocation}', 'edit-delete')">No</Button>`
        break

      //empty button set
      case "":
        //do nothing
        break
    }
  }
  //if the data doesn't exits 
  else
  {
    //if the dataCell is in the "instructions for my medication before my surgery"
    if(dataLocation_split[0] == "instructionsForMyMedicationBeforeMySurgery")
    {
      //delete it
      dataCell.parentNode.parentNode.removeChild(dataCell.parentNode)
    }
    //put a button to add data to the content
    content += `<div id="${dataCell.id+"ADD"}" onclick="addInputs(this.parentNode, '${dataLocation}')">
          add
      </div>`
  }
  //paste content into dataCell
  dataCell.innerHTML = content
}

/**
 * Adds input fields to the dataCell so that the user can modify their checklist, users dataLocation to decide which inputs to add and to fill in default values
 * 
 * @param {HTML Object} dataCell HTML object that the data is being inserted into
 * 
 * @param {String} dataLocation Location of the data in the CHECKLIST_DATA JSON Object. format: "{index1}->{index2}->{index3}->etc"
 */
function addInputs(dataCell, dataLocation)
{
  var getData_string = "" //used in the save button to get data from input fields
  var content = "" //used store content before it is pasted into the data cell
  //split the data location into an array
  var dataLocation_split = dataLocation.split("->")
  //check which section the input fields are in 
  switch(dataLocation_split[0])
  {
    //"main page" - date field
    case "mainPage":
      //set default values
      var value = ""
      //if actual data exists then replace the default values
      if(CHECKLIST_DATA.hasOwnProperty(dataLocation_split[0]) && CHECKLIST_DATA[dataLocation_split[0]].hasOwnProperty(dataLocation_split[1]))
      {
        value = CHECKLIST_DATA[dataLocation_split[0]][dataLocation_split[1]]["date"]
      }
      //add field with apropriate id and default value
      content += ` Date:<input id="${dataLocation}->date" value="${value}" type="date">`
      //using the same id set above create a string that when run a javascript will create a JSON object with the data from the input field in it
      getData_string += `{"date": document.getElementById("${dataLocation}->date").value}`
      break
    
    //"The Night Before & Morning of Surgery" section - time field
    case "theNightBeforeAndMorningOfMySurgery":
      //set default values
      var value = ""
      //if actual data exists then replace the default values
      if(CHECKLIST_DATA.hasOwnProperty(dataLocation_split[0]) && CHECKLIST_DATA[dataLocation_split[0]].hasOwnProperty(dataLocation_split[1]))
      {
        value = CHECKLIST_DATA[dataLocation_split[0]][dataLocation_split[1]]["time"]
      }
      //add field with apropriate id and default value
      content += ` Time:<input id="${dataLocation}->time" value="${value}" type="time">`
      //using the same id set above create a string that when run a javascript will create a JSON object with the data from the input field in it
      getData_string += `{"time": document.getElementById("${dataLocation}->time").value}`
      break

    //"If I am Having a Day Case Procedure (Discharge same day as surgery)" section - name, relationship and phonenumber fields
    case "ifIAmHavingADayCaseProcedure":
      //set default values
      var values = {"name": "", "relationship": "", "phonenumber": ""}
      //if actual data exists then replace the default values
      if(CHECKLIST_DATA.hasOwnProperty(dataLocation_split[0]) && CHECKLIST_DATA[dataLocation_split[0]].hasOwnProperty(dataLocation_split[1]))
      {
        values = CHECKLIST_DATA[dataLocation_split[0]][dataLocation_split[1]]
      }
      //add fields with apropriate id's and default values
      content += ` Name: <input id="${dataLocation}->name" value="${values["name"]}">
                          Relationship to you: <input id="${dataLocation}->relationship" value="${values["relationship"]}">
                          Phone number: <input id="${dataLocation}->phonenumber" value="${values["phonenumber"]}">`
                          //using the same id's set above create a string that when run a javascript will create a JSON object with all the data from the input fields in it
      getData_string += `{"name": document.getElementById("${dataLocation}->name").value,
                          "relationship": document.getElementById("${dataLocation}->relationship").value,
                          "phonenumber": document.getElementById("${dataLocation}->phonenumber").value}`
      break

    //"Things I need to know before I go home" section - date, time, doctor and location fields
    case "thingsINeedToKnowBeforeIGoHome":
      //check which specific cell is having inputs added to it (each is different)
      switch(dataLocation_split[1])
      {
        //"My follow up appointment" dataCell - fields for data, time, docator and location
        case "myFollowUpAppointment":
          //set default values
          var values = {"date": "", "time": "", "doctor": "", "location": ""}
          //if actual data exists then replace the default values
          if(CHECKLIST_DATA.hasOwnProperty(dataLocation_split[0]) && CHECKLIST_DATA[dataLocation_split[0]].hasOwnProperty(dataLocation_split[1]))
          {
            values = CHECKLIST_DATA[dataLocation_split[0]][dataLocation_split[1]]
          }
          //add fields with apropriate id's and default values
          content += ` Date: <input id="${dataLocation}->date" value="${values["date"]}" type="date">
                              Time: <input id="${dataLocation}->time" value="${values["time"]}" type="time">
                              Doctor: <input id="${dataLocation}->doctor" value="${values["doctor"]}">
                              location: <input id="${dataLocation}->location" value="${values["location"]}">`
          //using the same id's set above create a string that when run a javascript will create a JSON object with all the data from the input fields in it
          getData_string += `{"date": document.getElementById("${dataLocation}->date").value,
                              "time": document.getElementById("${dataLocation}->time").value,
                              "doctor": document.getElementById("${dataLocation}->doctor").value,
                              "location": document.getElementById("${dataLocation}->location").value}`
          break

        //"My wound dressings" dataCell - days to leave dressing and other fields
        case "myWoundDressings":
          //set default values
          var values = {"leaveForDays": "", "other": ""}
          //if actual data exists then replace the default values
          if(CHECKLIST_DATA.hasOwnProperty(dataLocation_split[0]) && CHECKLIST_DATA[dataLocation_split[0]].hasOwnProperty(dataLocation_split[1]))
          {
            values = CHECKLIST_DATA[dataLocation_split[0]][dataLocation_split[1]]
          }
          //add fields with apropriate id's and default values
          content += ` Leave wound dressing intact for <input id="${dataLocation}->leaveForDays" value="${values["leaveForDays"]}"> days.
                              Other: <input id="${dataLocation}->other" value="${values["other"]}">`
          //using the same id's set above create a string that when run a javascript will create a JSON object with all the data from the input fields in it
          getData_string += `{"leaveForDays": document.getElementById("${dataLocation}->leaveForDays").value,
                              "other": document.getElementById("${dataLocation}->other").value}`
          break

        //"My diet" dataCell - diet type and other fields
        case "myDiet":
          //set default values
          var values = {"type": "", "other": ""}
          //if actual data exists then replace the default values
          if(CHECKLIST_DATA.hasOwnProperty(dataLocation_split[0]) && CHECKLIST_DATA[dataLocation_split[0]].hasOwnProperty(dataLocation_split[1]))
          {
            values = CHECKLIST_DATA[dataLocation_split[0]][dataLocation_split[1]]
          }
          //add fields with apropriate id's and default values
          content += ` Type: <input id="${dataLocation}->type" value="${values["type"]}">
                              Other: <input id="${dataLocation}->other" value="${values["other"]}">`
          //using the same id's set above create a string that when run a javascript will create a JSON object with all the data from the input fields in it
          getData_string += `{"type": document.getElementById("${dataLocation}->type").value,
                              "other": document.getElementById("${dataLocation}->other").value}`
          break

        //"My activity levels" dataCell - fields for when the petient ccan start work, driving, exersie and heavy lifting
        case "myActivityLevels":
          //set default values
          var values = {"goBackToWork": {"date": ""}, "startDriving": {"date": ""}, "startExercising": {"date": ""}, "liftHeavierObjects": {"date": ""}}
          //if actual data exists then replace the default values
          if(CHECKLIST_DATA.hasOwnProperty(dataLocation_split[0]) && CHECKLIST_DATA[dataLocation_split[0]].hasOwnProperty(dataLocation_split[1]))
          {
            values = CHECKLIST_DATA[dataLocation_split[0]][dataLocation_split[1]]
          }
          //add fields with apropriate id's and default values
          content += `
          When I can:
          <ul>
              <li>
                  Go back to work: <input id="${dataLocation}->goBackToWork" value="${values["goBackToWork"]["date"]}" type="date">
              </li>
              <li>
                  Start driving: <input id="${dataLocation}->startDriving" value="${values["startDriving"]["date"]}" type="date">
              </li>
              <li>
                  Start exercising: <input id="${dataLocation}->startExercising" value="${values["startExercising"]["date"]}" type="date">
              </li>
              <li>
                  Life heavier objects: <input id="${dataLocation}->liftHeavierObjects" value="${values["liftHeavierObjects"]["date"]}" type="date">
              </li>
          </ul>`
          //using the same id's set above create a string that when run a javascript will create a JSON object with all the data from the input fields in it
          getData_string += `{"goBackToWork": {"date": document.getElementById("${dataLocation}->goBackToWork").value},
                              "startDriving": {"date": document.getElementById("${dataLocation}->startDriving").value},
                              "startExercising": {"date": document.getElementById("${dataLocation}->startExercising").value},
                              "liftHeavierObjects": {"date": document.getElementById("${dataLocation}->liftHeavierObjects").value}}`
          break
        }
      break

    //"Instructions for My Medication Before Surgery" section - fields for medication name and when to stop said medication
    case "instructionsForMyMedicationBeforeMySurgery":
      //set default values
      var values = {"name": "", "whenToStop": ""}
      //if actual data exists then replace the default values
      if(CHECKLIST_DATA.hasOwnProperty(dataLocation_split[0]) && CHECKLIST_DATA[dataLocation_split[0]].hasOwnProperty(dataLocation_split[1]))
      {
        values = CHECKLIST_DATA[dataLocation_split[0]][dataLocation_split[1]]
      }
      //add fields with apropriate id's and default values to the right cell
      content += `
      <input id="${dataLocation_split[0]}->${dataLocation_split[1]}->whenToStop" value="${values["whenToStop"]}"> days before surgery`
      //add fields with apropriate id's and default values to the left cell
      document.getElementById(`${dataLocation_split[1]}->name`).innerHTML = `
      <input id="${dataLocation_split[0]}->${dataLocation_split[1]}->name" value="${values["name"]}">`
      //using the same id's set above create a string that when run a javascript will create a JSON object with all the data from the input fields in it
      getData_string += `{"name": document.getElementById("${dataLocation_split[0]}->${dataLocation_split[1]}->name").value,
                          "whenToStop": document.getElementById("${dataLocation_split[0]}->${dataLocation_split[1]}->whenToStop").value}`
      break

    //"My Appointments Before Surgery" and "2 – 3 Weeks Before My Surgery" sections
    default:
      //set default values
      var values = {"date": "", "time": "", "location": ""}
      //if actual data exists then replace the default values
      if(CHECKLIST_DATA.hasOwnProperty(dataLocation_split[0]) && CHECKLIST_DATA[dataLocation_split[0]].hasOwnProperty(dataLocation_split[1]))
      {
        values = CHECKLIST_DATA[dataLocation_split[0]][dataLocation_split[1]]
      }
      //add fields with apropriate id's and default values
      content += ` Date: <input id="${dataLocation}->date" value="${values["date"]}" type="date">
                          Time: <input id="${dataLocation}->time" value="${values["time"]}" type="time">
                          location: <input id="${dataLocation}->location" value="${values["location"]}">`
      //using the same id's set above create a string that when run a javascript will create a JSON object with all the data from the input fields in it
      getData_string += `{"date": document.getElementById("${dataLocation}->date").value,
                          "time": document.getElementById("${dataLocation}->time").value,
                          "location": document.getElementById("${dataLocation}->location").value}`
      break
  }
  //replace all double quotes in the string with HTML Charater entity quotes - this is to prevent confics with other double quotes and is used instead of escaping string
  getData_string = getData_string.replace(/"/g, '&quot;')
  //add save and cancel buttons to the content
  //save button uses the updateChecklistData() function to save new info and then the displayChecklistData() function to display this data
  //cancel button uses displayChecklistData to display old data
  content += `
  <button onclick="updateChecklistData('${dataLocation}', ${getData_string});displayChecklistData(this.parentNode, '${dataLocation}', 'edit-delete')">Save</button>
  <button onclick="displayChecklistData(this.parentNode, '${dataLocation}', 'edit-delete')">Cancel</button>`
  //paste content into cell
  dataCell.innerHTML = content
}

/**
 * Updates the data given by replacing the old values found through the dataLocation variable or creating new values there if none were found.
 * Replacing data with null causes the function to delete the data instead.
 * 
 * @param {String} dataLocation Sting specifying where the data to be saved is. Format "{index1}->{index2}->{index3}->etc"
 * 
 * @param {JSON Object} data JSON object of the new data to be save, passing in null will instead cause the function to delete the old data
 */
function updateChecklistData(dataLocation, data)
{
  //split the data location into an array
  dataLocation_split = dataLocation.split("->")
  //if data was given
  if(data != null)
  {
    // if the number of indexes in dataLocation is 2 or more. Almost allways 2, only 3 with the "Instructions for My Medication Before Surgery" section in which case the 3rd index can be ignored
    if(dataLocation_split.length >= 2)
    {
      //if the data doesn't already exist
      if(!CHECKLIST_DATA.hasOwnProperty(dataLocation_split[0]))
      {
        //create a temporary JSON String with the data and last index
        var temp = `{"${dataLocation_split[1]}": ${JSON.stringify(data)}}`
        //parse the string and add it to the CHECKLIST_DATA JSON Object
        CHECKLIST_DATA[dataLocation_split[0]] = JSON.parse(temp) 
      } 
      //if the data does exist replace the old data with the new
      else
      {
        CHECKLIST_DATA[dataLocation_split[0]][dataLocation_split[1]] = data
      }
    }
    //if the number of indexes in dataLocation is 1
    else if(dataLocation_split.length == 1)
    {
      //this command both replaces old data if it exists and creates the index for the data if it didn't already exist
      CHECKLIST_DATA[dataLocation_split[0]]= data 
    }
  }
  //if null was given as the data
  else
  {
    // if the number of indexes in dataLocation is 2 or more. Almost allways 2, only 3 with the "Instructions for My Medication Before Surgery" section in which case the 3rd index can be ignored
    if(dataLocation_split.length >= 2)
    {
      //delete the data
      delete CHECKLIST_DATA[dataLocation_split[0]][dataLocation_split[1]]
    }
    //if the number of indexes in dataLocation is 1
    else if(dataLocation_split.length == 1)
    {
      //delete the data
      delete CHECKLIST_DATA[dataLocation_split[0]]
    }
  }

  //create a new request to update the checklist data on the server
  var checklistUpdate_req = new XMLHttpRequest() 
  var url = domain + "/json/setchecklist"
  checklistUpdate_req.open("POST", url, true)
  checklistUpdate_req.setRequestHeader("Content-Type", "application/json")
  //set data to a JSON string containing the users URN, passowrord and new checklist data
  data = JSON.stringify({"urn": URN, "password": PASSWORD, "checklist":CHECKLIST_DATA})
  //create function that handels the request being completed
  checklistUpdate_req.onreadystatechange = function ()
  {  
    //if the request is not finished ignore update
    if(checklistUpdate_req.readyState != 4)
    {
      return
    }
    //if the update was successful display success message
    if (!checklistUpdate_req.status === 200) 
    {
      ERRORS += "checklists failed to update: " + checklistUpdate_req.status + " - " + checklistUpdate_req.responseText
    }
  }
  //send request
  checklistUpdate_req.send(data)
}

/**
 * Adds a new field to the "Instructions for My Medication Before Surgery" section
 */
function addMedication()
{
  if(!CHECKLIST_DATA.hasOwnProperty("instructionsForMyMedicationBeforeMySurgery"))
  {
    CHECKLIST_DATA["instructionsForMyMedicationBeforeMySurgery"] = []
  }
  //get index of new medication
  var index = CHECKLIST_DATA["instructionsForMyMedicationBeforeMySurgery"].length
  //increment index
  CHECKLIST_DATA["instructionsForMyMedicationBeforeMySurgery"].length += 1
  //get tavle
  var table = document.getElementsByTagName("table")[0]
  //insert row at the bottom just below the add medication button
  var row = table.insertRow(table.rows.length - 1)
  //add the two cells to the row
  var leftCell = row.insertCell(0)
  var rightCell = row.insertCell(1)
  //set the class names so that they can be found by other code
  leftCell.className =  "checklistSectionData"
  rightCell.className =  "checklistSectionData"
  //set the id's so that the addInputs() function call can find them
  leftCell.id = index + "->name"
  rightCell.id = index + "->whenToStop"
  //use the addInputs() function to add fields to ender data to the new cells
  addInputs(rightCell, `instructionsForMyMedicationBeforeMySurgery->${index}->whenToStop`)
}

/**
 * Reformats a date from yyyy-mm-dd to dd/mm/yyy
 * 
 * @param {String} date string containing a date in the format yyyy-mm-dd
 */
function reformatDate(date)
{
  //split the data up into each component
  var temp = date.split("-")
  //return the reordered conponents
  return `${temp[2]}/${temp[1]}/${temp[0]}`
}

/**
 * reformats a time from 24 hour to 12 hour format
 * 
 * @param {String} time string containing the time in the format hh:mm (24 hour)
 */
function reformatTime(time)
{
  // split the time into each component
  var temp = time.split(":")
  // get the hours as a number
  var hours_int = parseInt(temp[0])
  //set the default sufix to AM
  var sufix = "AM"
  //if the hours is 0 (early morning) set the sting version to 12
  if(hours_int == 0)
  {
    temp[0] = "12"
  }
  // if the hours is 12 set the suffix to PM
  else if(hours_int == 12)
  {
    sufix = "PM"
  }
  // if the hours is more than 12
  else if(hours_int > 12)
  {
    //reduce the hours to 12
    hours_int -= 12
    // set the string version
    if(hours_int < 10)
    {
      temp[0] = "0" + hours_int.toString()
    }
    else
    {
      temp[0] = hours_int.toString()
    }
    // set the suffix to PM
    sufix = "PM"
  }
  //return the modified time
  return `${temp[0]}:${temp[1]} ${sufix}`
}