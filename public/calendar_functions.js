const date = new Date();

const renderCalendar = () => {

	date.setDate(1); //Sets the date to

	const current_month = date.getMonth(); // Stores the current month

	const current_date = new Date().toDateString(); //Stores the current date

	const month = ["January","February","March","April","May","June","July","August","September","October","November","December"];

	const monthDays = document.querySelector(".days");

	const lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate(); //Gets the lastday of the current month

	const firstOfMonth = date.getDay(); //Gets the index of the first day of the month (0 = Sunday, 1 = Monday, 2 = Tuesday, so on)

	const lastOfMonth = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDay(); // Gets the index of the last day of the current month (0 = Sunday, 1 = Monday, 2 = Tuesday, so on)

	const prevLastDay = new Date(date.getFullYear(), date.getMonth(), 0).getDate(); //Gets the last day of the previous month

	const nextDays = 7 - lastOfMonth - 1; //Calculates the humber of first days of next month to create to fill out calendar nicely.

	document.querySelector(".date h1").innerHTML = month[current_month]; //Grabs the header 1 section of the date div in calender.html and fills it with the current month name

	document.querySelector(".date p").innerHTML = current_date; // Grabs the paragraph section of the date div in calendar.html and fills it with the current date

	let days = ""; //Create an empty string variable to be filled with divs

	/*
	To format calendar nicely we need to include last few days of previous month
	For loop to adding last few days of previous month. x = the index of the first day of the month.
	Therefore if x = 6, it would mean the first day of that particular month started on a Saturday, meaning we need to fill out the previous sunday -> friday.
	*/
	for(let x = firstOfMonth; x > 0; x--){
		days += `<div class = "prev-date">${prevLastDay - x + 1}</div>`;
	}

	//This for loop sets up all the days in the current month, and will also set the div class to today, if the date being created is the current day
	for(let i = 1; i <= lastDay; i++){
		if(i === new Date().getDate() && date.getMonth() === new Date().getMonth()){
			days += `<div class = "today">${i}</div>`;
		}else{
			days += `<div>${i}</div>`;
		}
	}


	//This for loop is responsible for adding in the first few days of the next month
	if(nextDays >= 1)
    {
        for(let j = 1; j <= nextDays; j++){
            days += `<div class="next-date">${j}</div>`;
            monthDays.innerHTML=days;
        }
    }
    else
    {
        for(let k = 0; k <= nextDays; k++){
            days += `<div class="invisible"></div>`;
            monthDays.innerHTML=days;
        }
    }
};

/*
This function is used by the buttons on the calendar, it essentially just changes the the current month
to either the next or previous month depending on which arrow was clicked. Aftwerwards it recalculates
all of the above to render the calendar again.
*/
function changeMonth(inc)
{
    date.setMonth(date.getMonth()  + inc);
    renderCalendar();
}


function displayCalendar()
{
  //scroll to top of page
  window.scrollTo(0,0)

  //send a request for the contact us content
  var response = synchronousLoad("/calendar.html")

  //if the content was receive display it otherwise display and error
  var calendar_content = ""
  if(response != null)
  {
    calendar_content = response
  }
  else
  {
    ERRORS += "couldn't get contact us page <br><br>"
    calendar_content = "Error displaying contact us:<br><br><br>" + ERRORS
  }
  document.getElementById("content").innerHTML = calendar_content
  renderCalendar()
  changeMonth(-1)
  changeMonth(1)
}
