/**
 * deselects all header buttons then selects the clicked button. also calls relavant functions
 * for changing page contents
 *
 * @param headerButton used so the function knows what button was clicked, button-on-clicked event must
 * pass in itself
 */
function headerClick(headerButton)
{
  //get all header buttons and deselect them
  var buttons = document.getElementsByClassName("navButton")
  for(i = 0; i < buttons.length; i++)
  {
    buttons[i].style = "color: black"
  }
  //select the clicked button
  headerButton.style = "color:purple"
  //update current_article_section_no
  switch(headerButton.id)
  {
    case "optimisemyhealth":
      current_article_subsection_no = 0
      break

    case "beforemysurgery":
      current_article_subsection_no = 1
      break

    case "aftermysurgery":
      current_article_subsection_no = 2
      break

	  case "aboriginalandtorresstraitislanders":
      current_article_subsection_no = 3
      break
  }

  displayArticleThumbnails(current_article_subsection_no)
}

/**
 * function takes in the current section number and displays articles to the
 * screen, erases all previous content
 *
 * @param {int} article_subsection_no number of the article section 0 = optimise my health, 1 = before
 * my surgery and 2 = after my surgery, 3 = indigenous and torres strait islander, 4 = about us
 */
function displayArticleThumbnails(article_subsection_no)
{
  //scroll to top of page
  window.scrollTo(0,0)

  var content = ""
  //add first article without divider above it
  if(articles[article_subsection_no].length > 0)
  {
    content += `
      <div class="articleLink" id="${0}" onclick="articleLinkClick(${0}, ${article_subsection_no})">
        <div class="articleLinkThumbnailContainer">
          <img class="articleLinkThumbnail" src="${domain}/articles/${sections[article_subsection_no]}/${articles[article_subsection_no][0][1]}/thumbnail.jpg">
        </div>
        <div class="articleLinkTitleContainer">
          <h1 class="articleLinkTitle">
            ${articles[article_subsection_no][0][0]}
          </h1>
        </div>
      </div>`
    //add all other articles
    for(var article_no = 1; article_no < articles[article_subsection_no].length; article_no++)
    {
      content += `
      <hr class="articleLinkDivider">

      <div class="articleLink" id="${article_no}" onclick="articleLinkClick(${article_no}, ${article_subsection_no})">
        <div class="articleLinkThumbnailContainer">
          <img class="articleLinkThumbnail" src="${domain}/articles/${sections[article_subsection_no]}/${articles[article_subsection_no][article_no][1]}/thumbnail.jpg">
        </div>
        <div class="articleLinkTitleContainer">
          <h1 class="articleLinkTitle">
            ${articles[article_subsection_no][article_no][0]}
          </h1>
        </div>
      </div>`
    }
    document.getElementById("content").innerHTML = content
  }
  else
  {
    document.getElementById("content").innerHTML = "Error displaying articles:<br><br><br>" + ERRORS
  }
}


/**
 * takes in the article and section number of a given article and
 * displays it
 *
 * @param {int} article_no number of the article counting from zero down the screen
 * @param {int} section_no number of the section 0 = optimise my health, 1 = before
 * my surgery and 2 = after my surgery
 */
function articleLinkClick(article_no, section_no)
{
  moved = true;
  //scroll to top of page
  window.scrollTo(0,0)

  //hide title bar and article sections, show article header, adjust headerSpacer to fit
  document.getElementById("navbar").style.visibility = "hidden"
  document.getElementById("titlebar").style.visibility = "hidden"
  document.getElementById("articleHeader").style.visibility = "visible"
  document.getElementById("searchBarHeader").style.visibility = "hidden"
  document.getElementById("headerSpacer").style.paddingBottom = "12.5%"

  //insert relevant variables into content template
  content = `
    <img src="${domain}/articles/${sections[section_no]}/${articles[section_no][article_no][1]}/thumbnail.jpg">
    <div id="articleTop">
      <h1 id="articleTitle">
        ${articles[section_no][article_no][0]}
      </h1>
    </div>

    <hr id="articleDivider">

    <div class="articleBody" id="articleBodyContainer">
    ${articles[section_no][article_no][2]}
    </div>`

  //insert content into content container
  document.getElementById("content").innerHTML = content
}
