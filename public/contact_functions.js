/**
 * gets and displays the contact us page
 */
function displayContactus()
{
  //scroll to top of page
  window.scrollTo(0,0)

  //send a request for the contact us content
  var response = synchronousLoad("/contactus.html")

  //if the content was receive display it otherwise display and error
  var contactus_content = ""
  if(response != null)
  {
    contactus_content = response
  }
  else
  {
    ERRORS += "couldn't get contact us page <br><br>"
    contactus_content = "Error displaying contact us:<br><br><br>" + ERRORS
  }
  document.getElementById("content").innerHTML = contactus_content
}

/**
 * displays the specified contact us section
 * @param {String} section section to be displayed, one of: {switchboard, elective, managers, preadmission}
 */
function displayContactSection(section)
{
  //scroll to top of page
  window.scrollTo(0,0)

  //send a request for the sections content
  var response = synchronousLoad("/contactSections/" + section + ".html")
  //if the content was receive display it otherwise display an error
  contactSection_content = ""
  if(response != null)
  {
    contactSection_content = response
  }
  else
  {
    contactSection_content = "error getting contact details"
    console.log("couldn't get contact page")
  }
  document.getElementById("content").innerHTML = contactSection_content

  //modify the header to suit the new screen
  document.getElementById("titlebar").style.visibility = "hidden"
  document.getElementById("contactSectionHeader").style.visibility = "visible"
  document.getElementById("headerSpacer").style.paddingBottom = "12.5%"
}

/**
 * used by back button on contact us subsections, displays contact us page
 */
function contactBackClick()
{
  //modify the header to suit the new screen
  document.getElementById("titlebar").style.visibility = "visible"
  document.getElementById("contactSectionHeader").style.visibility = "hidden"
  document.getElementById("headerSpacer").style.paddingBottom = "13.75%"
  //display the contact us screen again
  displayContactus()  
}

/**
 * loads the phonenumber given into the users phone
 */
function phone(number)
{
  window.open("tel:" + number)
}

/**
 * loads the email address given into the users phone
 */
function email(address)
{
  window.open("mailto:" + address)
}