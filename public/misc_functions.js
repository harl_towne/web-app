//variables used to keep track of  the current area the app is in
//used to allow the back button to decide where to send the user back to
//used to display the correct articles
var current_article_subsection_no = 0
var current_checklist_section = null
var current_section = "surgery"
var moved = false

/**
 * Returns the resouce from the url specified. Uses the "url_lookup" variable to ensure the pages are not loaded over the web more than once.
 *
 * @param {Strng} url url to retrieve
 */
var url_lookup = {}
function synchronousLoad(url)
{
  //check if the page has already be loaded
  if(url_lookup.hasOwnProperty(url))
  {
    //if so return that page
    return url_lookup[url]
  }
  //if not create a request for that page
  var request = new XMLHttpRequest()
  request.open('GET', domain + url, false)
  request.send(null)
  //if the request was successful
  if(request.status == 200)
  {
    //store the page for future
    url_lookup[url] = request.responseText
    //and return page
    return request.responseText
  }
  //otherise return null
  else
  {
    return null
  }
}

/**
 * deselects all footer buttons then selects the clicked button. also calls relavant functions
 * for changing page contents
 *
 * @param {object} footerButton used so the function knows what button was clicked, button-on-clicked event must
 * pass in itself
 */
function footerClick(footerButton)
{
  //get all footer buttons and deselect them
  var buttons = document.getElementsByClassName("footerButton")
  for(var i = 0; i < buttons.length; i++)
  {
    buttons[i].style = "background: rgba(0, 0, 0, 0.3)"
  }
  //select the clicked button
	footerButton.style = "background: rgba(0, 0, 0, 0)"

  //check if the surgery button was the button that was clicked
  if(footerButton.id === "surgery")
  {
    //if so make the header buttons visible
    document.getElementById("navbar").style.visibility = "visible"
    document.getElementById("titlebar").style.visibility = "visible"
    document.getElementById("articleHeader").style.visibility = "hidden"
    document.getElementById("searchBarHeader").style.visibility = "hidden"
    document.getElementById("headerSpacer").style.paddingBottom = "25%"

  }
  else
  {
    //if not make the header buttons invisible
    document.getElementById("navbar").style.visibility = "hidden"
    document.getElementById("titlebar").style.visibility = "visible"
    document.getElementById("articleHeader").style.visibility = "hidden"
    document.getElementById("searchBarHeader").style.visibility = "hidden"
    document.getElementById("headerSpacer").style.paddingBottom = "13.75%"
  }
  document.getElementById("contactSectionHeader").style.visibility = "hidden"

  //check which button was clicked and display that section
  switch(footerButton.id)
  {
    case "surgery":
      current_section = footerButton.id
      displayArticleThumbnails(current_article_subsection_no)
      break

    case "checklists":
	    current_section = footerButton.id
      displayChecklists()
      break

    case "aboutus":
	    current_section = footerButton.id
      displayArticleThumbnails(4)
      break

    case "calendar":
      current_section = footerButton.id
      displayCalendar()
      break

    case "contactus":
	    current_section = footerButton.id
      displayContactus()
      break

    //for invalid buttons/buttons that haven't been coded just clear the screen
    default:
      document.getElementById("content").innerHTML = ""
      break
  }
}

/**
 * exits out of the article and returns to the article list
 */
function backButtonClick()
{
	if(current_section === "checklists" && moved && current_checklist_section != null){
		moved = false;
		returntoChecklist()
	}else if(current_section === "checklists" && !moved && current_checklist_section == null){
	  footerClick(document.getElementById("surgery"))
	}
	else if(current_section === "checklists" && moved && current_checklist_section == null){
	  footerClick(document.getElementById("surgery"))
	}
	else{
		footerClick(document.getElementById(current_section))
	}
}

/**
 * changes the font by the given amount
 *
 * @param {float} change amount the change the font size by in vw
 */
function changeTextSize(change)
{


  //get pixel size of text and convert to number
  var px_size = getComputedStyle(document.getElementById("content")).fontSize
  px_size = px_size.slice(0, px_size.length-2)

  //get width of screen
  var screen_width = Math.max(window.innerWidth || 0, document.documentElement.clientWidth)

  //convert to vw
  var vw_size = px_size/(screen_width/100)

  //set font size to the current size plus the change
  document.getElementById("content").style.fontSize = vw_size + change + "vw"
}
