each article section (optimize my health, before my surgery, after my surgery, indigenous and torres strait islander) has its own folder and all the articles in that section go in that folder.

each section folder has an info.txt file, this file contains the title, date and folder/file name of all the articles in the section separated by a semicolon

each article is contained inside of a folder that is call the same as the articles html documents name

each article folder contains an image and a html document

the image must be called thumbnail.jpg and must have an aspect ration of roughly 26:19 (image  height is 72.92% of the width) images that are not this aspect ratio will begin to be cut off

the html document contains the article contents and must have the same name as the title