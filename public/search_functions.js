/**
 * function for displaying the search page when clicked
 */
function searchButtonClick()
{
  //hide title bar and article sections, show article header, adjust headerSpacer to fit
  document.getElementById("navbar").style.visibility = "hidden"
  document.getElementById("titlebar").style.visibility = "hidden"
  document.getElementById("articleHeader").style.visibility = "hidden"
  document.getElementById("searchBarHeader").style.visibility = "visible"  
  document.getElementById("headerSpacer").style.paddingBottom = "12.5%"

  //call search function with value in search bar (empty by default)
  searchArticles(document.getElementById("searchBar").value)
}

/**
 * Function for searching through articles. Takes a search string as a parameter and returns a list of articles whoes titles
 * contain that string (case insensitive) sorted based on how early the search string appears in the article title (earlier is sorted first)
 * 
 * @param {String} searchString string to match in article titles
 */
function searchArticles(searchString)
{
  //if there is no search string give a message for the user to enter one
  if(searchString == "")
  {
    document.getElementById("content").innerHTML = "Enter something into the search bar to view articles."
    return
  }

  //convert search string to lowwer case
  searchString = searchString.toLowerCase()
  //array to store matches
  var matches = []
  //loop through all articles in all sections
  for(var section_no = 0; section_no < sections.length; section_no++)
  {
    for(var article_no = 0; article_no < articles[section_no].length; article_no++)
    {
      //find the first occurence of the search string in the articles title
      var index = articles[section_no][article_no][0].toLowerCase().indexOf(searchString)
      //if the search string does exist in the title then add that article and the index of the match to the matches array
      if(index >= 0)
      {
        matches.push([articles[section_no][article_no], section_no, article_no, index])
      }
    }
  }
  //sort matches so that articles where the search string appears early in the title are sorted first in the array
  matches.sort(function compareMatches(a, b){
    return a[3] - b[3]
  })

  //scroll to top of page
  window.scrollTo(0,0)

  var content = ""
  if(matches.length > 0)
  {
    //add first article without divider above it
    content += `
      <div class="articleLink" id="${0}" onclick="articleLinkClick(${matches[0][2]}, ${matches[0][1]})">
        <div class="articleLinkThumbnailContainer">
          <img class="articleLinkThumbnail" src="${domain}/articles/${sections[matches[0][1]]}/${matches[0][0][1]}/thumbnail.jpg">
        </div>
        <div class="articleLinkTitleContainer">
          <h1 class="articleLinkTitle"> 
            ${matches[0][0][0]} 
          </h1>
        </div>
      </div>`
    //add all other articles
    for(var i = 1; i < matches.length; i++)
    {
      content += `
      <hr class="articleLinkDivider">

      <div class="articleLink" id="${i}" onclick="articleLinkClick(${matches[i][2]}, ${matches[i][1]})">
        <div class="articleLinkThumbnailContainer">
          <img class="articleLinkThumbnail" src="${domain}/articles/${sections[matches[i][1]]}/${matches[i][0][1]}/thumbnail.jpg">
        </div>
        <div class="articleLinkTitleContainer">
          <h1 class="articleLinkTitle"> 
            ${matches[i][0][0]} 
          </h1>
        </div>
      </div>`
    }
    //paste content to page
    document.getElementById("content").innerHTML = content
  }
  //if there are no matching articles display an error message
  else
  {
    document.getElementById("content").innerHTML = "No matching articles."
  }
}

/**
 * function for clearing the search bar and search results
 */
function clearSearchBar()
{
  document.getElementById('searchBar').value = ''
  searchArticles("")
}