//stores URN, password and checklist data of loged in user - also used by checklist_functions
var URN = null
var PASSWORD = null
var CHECKLIST_DATA = null

/**
 * functon for displaying a user login/out related page when clicked
 */
function userButtonCLick()
{
  //hide title bar and article sections, show article header, adjust headerSpacer to fit
  document.getElementById("navbar").style.visibility = "hidden"
  document.getElementById("titlebar").style.visibility = "hidden"
  document.getElementById("articleHeader").style.visibility = "visible"
  document.getElementById("searchBarHeader").style.visibility = "hidden"
  document.getElementById("headerSpacer").style.paddingBottom = "12.5%"

  //start request for user page
  var url = ""
  //get the login page if the user isn't logged in
  var profile = false
  if(URN == null || URN == "" || PASSWORD == null || PASSWORD == "")
  {
    url += "/user/login.html"
  }
  //if the user is loged it
  else
  {
    //get the profile page
    url += "/user/profile.html"
    //set profile page flag to true
    profile = true
    //check if the current browser supports notifications and if the permision hasn't been already denied
    if ('serviceWorker' in navigator && Notification.permission != "denied") {
      //call function to register service worker (script that will handel notifications client side)
      console.log('Registering service worker');
      registerServiceWorker().catch(error => console.error(error)); //function is asyncronous
    }
  }
  //send request
  var response = synchronousLoad(url)
  //if the request is successful paste the content to the page
  var userPage_content = ""
  if(response != null)
  {
    userPage_content = response
  }
  //otherwise paste an error message
  else
  {
    userPage_content = "failed to get page"
  }
  document.getElementById("content").innerHTML = userPage_content

  if(profile)
  {
    document.getElementById("notificationStatus").innerHTML = "Notification Status: " + Notification.permission
  }
}

async function registerServiceWorker() {
  //get worker from server and register it
  const registration = await navigator.serviceWorker.register('/worker.js', {scope: '/'})

  navigator.serviceWorker.ready.then((registration) =>
  {
    subscribeUser(registration)
  })
}

async function subscribeUser(workerRegistration)
{
  //subsribe worker to notifications from server
  const subscription = await workerRegistration.pushManager.subscribe({
      userVisibleOnly: true,
      applicationServerKey: urlBase64ToUint8Array(publicVapidKey)
    }).catch((err) => {
      console.error(err)
    })
  console.log('Registered push')

  //update notification status for user
  document.getElementById("notificationStatus").innerHTML = "Notification Status: " + Notification.permission

  //triger server to send push notification to client (test)
  var data = JSON.stringify({"subscription": subscription, "urn":URN, "password":PASSWORD})
  var subscription_req = new XMLHttpRequest()
  var url = domain + '/json/subscribe'
  subscription_req.open("POST", url, false)
  subscription_req.setRequestHeader("Content-Type", "application/json")
  subscription_req.send(data)
  console.log('Sent subscription')
}

// Boilerplate borrowed from https://www.npmjs.com/package/web-push#using-vapid-key-for-applicationserverkey
function urlBase64ToUint8Array(base64String) {
  const padding = '='.repeat((4 - base64String.length % 4) % 4)
  const base64 = (base64String + padding)
    .replace(/\-/g, '+')
    .replace(/_/g, '/')

  const rawData = window.atob(base64)
  const outputArray = new Uint8Array(rawData.length)

  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i)
  }
  return outputArray
}

/**
 * displays the user signup page
 */
function displaySignup()
{
  //send request for signup page
  var response = synchronousLoad("/user/signup.html")

  //if the request is successful paste the content to the page
  var userPage_content = ""
  if(response != null)
  {
    userPage_content = response
  }
  //otherwise paste an error message
  else
  {
    userPage_content = "failed to get page"
  }
  document.getElementById("content").innerHTML = userPage_content
}

/**
 * registers a new user
 *
 * @param {String} username users username
 * @param {String} password users password
 * @param {String} urn users phone number (used for login)
 * @param {String} email users email (optional)
 */
function registerUser(username, password, password2, urn, email, phonenumber)
{
  //check that all details exist and that the passwords match
  if(password != password2) //check both passwords are ths same
  {
    document.getElementById("errorMessage").innerHTML = "Passwords do not match."
    return
  }
  if(password == "") //check that a password has been entered
  {
    document.getElementById("errorMessage").innerHTML = "Please enter a password."
    return
  }
  if(urn == "") //check that a URN has been entered
  {
    document.getElementById("errorMessage").innerHTML = "Please enter a URN."
    return
  }
  if(email == "") //check that an email has been entered
  {
    document.getElementById("errorMessage").innerHTML = "Please enter a email."
    return
  }
  if(phonenumber == "") //check that a phone number has been entered
  {
    document.getElementById("errorMessage").innerHTML = "Please enter a phone number."
    return
  }
  if(username == "") //check that a username has been entered
  {
    document.getElementById("errorMessage").innerHTML = "Please enter a name."
    return
  }
  //create new post request to send details to server
  var req = new XMLHttpRequest();
  var url = domain + "/json/register"
  req.open("POST", url, true) //async
  req.setRequestHeader("Content-Type", "application/json")
  req.onreadystatechange = function () //function that runs when the request is updates
  {
    //if the request is not finished ignore update
    if(req.readyState != 4)
    {
      return
    }
    //if the request was successfull log the user in and send them to the profile page
    if (req.status === 200)
    {
      var register_res = JSON.parse(req.responseText)
      URN = register_res["urn"]
      PASSWORD = register_res["password"]
      userButtonCLick()
    }
    //if the request was unsuccessful print and error message on the screen
    else
    {
      document.getElementById("errorMessage").innerHTML = "An account for that URN already exists."
    }
  }
  //data to be send to server
  var data = JSON.stringify({"username": username, "password": password, "urn": urn, "email": email, "phonenumber": phonenumber});
  //send data and open request
  req.send(data)
}

/**
 * authenticates user - currently only used to check if login is correct, clientside will then
 * resend urn and password with each request for re-authentication. replace with sessionIDs sometime
 *
 * @param {String} urn the phone number the user entered
 * @param {String} password the password the user entered
 */
function loginUser(urn, password)
{
  //create and new post request for sending the user details
  var register_req = new XMLHttpRequest();
  var url = domain + "/json/login"
  req.open("POST", url, true)
  req.setRequestHeader("Content-Type", "application/json")
  req.onreadystatechange = function ()  //function that runs when the request is updates
  {
    //if the request is not finished ignore update
    if(req.readyState != 4)
    {
      return
    }
    //if the request was successfull log the user in and send them to the profile page
    if (req.status === 200)
    {
      var register_res = JSON.parse(req.responseText)
      URN = register_res["urn"]
      PASSWORD = register_res["password"]
      userButtonCLick()
    }
    //if the request was unsuccessful print and error message on the screen
    else
    {
      document.getElementById("errorMessage").innerHTML = "Incorrect URN or Password."
    }
  }
  //data to be send to server
  var data = JSON.stringify({"urn": urn, "password": password});
  //send data and open request
  req.send(data)
}

/**
 * sign out loged it user
 */
function signOut()
{
  //remove URN and password
  URN = null
  PASSWORD = null
  CHECKLIST_DATA = null
  //got back to sign in page
  userButtonCLick()
}
