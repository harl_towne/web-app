//stores erros for display on mobile - used across mutiple scripts
var ERRORS = ""

//domain that the website is currently being hosted at
//used to make requests to make web requests
//used across multiple scripts
var domain_temp = ""
if(window.location.hostname === "localhost")
{
  domain_temp = "http://" + window.location.hostname +  ":" + window.location.port
}
else
{
  domain_temp = "https://" + window.location.hostname
}
const domain = domain_temp

//names of each of the sections for the articles (must match folder names)
//also used is seach and article functions
const sections = ["optomisemyhealth", "beforemysurgery", "aftermysurgery", "aboriginalandtorresstraitislanders", "aboutus"]

//array for storing all info about articles index is [section number][articles number][0 - title, 2 - article content]
//also used is search and article functions
var articles = []
//insert section arrays into articles array
for(var i = 0; i < sections.length; i++)
{
  articles.push([]) 
}

//article loading is done asynchronously, this JSON object is used to allow the callback functions of the requests to figure out what to do with the server response
var url_index_lookup = {}

//for each section
for(var i = 0; i < sections.length; i++)
{
  //create request for info file
  var req = new XMLHttpRequest()
  var url = domain + "/articles/" +  sections[i] + "/info.txt"
  req.open("GET", url, true)
  url_index_lookup[url] = i //store section number in lookup variable for callback table
  req.onreadystatechange = function () //function that runs when the request is updated
  {   
    //if the request is not finished ignore update
    if(this.readyState != 4)
    {
      return
    }
    //if the request was successfull
    if (this.status === 200)
    {
      //get section number from lookup table
      var i = url_index_lookup[this.responseURL]

      //split info by line
      var info = this.responseText.split("\n")
      //for each line
      for(var j = 0; j < info.length; j++)
      {
        //skip itteration if line is empty
        if(info[j] == "")
        {
          continue
        }
        //split info by semicolen to get the title/folder name and date
        var temp = info[j].split(";")
        var title = temp[0].trim()
        var folder = temp[1].trim()

        //add the article title and folder to the articles variable
        articles[i].push([title, folder])
        //create request for article content
        var nreq = new XMLHttpRequest()
        var url = domain + "/articles/" +  sections[i] + "/" + folder + "/" + folder + ".html"
        url_index_lookup[url] = [i, j] //store section and article number in lookup variable for callback function
        nreq.open("GET", url, true)
        nreq.onreadystatechange = function () //function that runs when the request is updated
        {   
          //if the request is not finished ignore update
          if(this.readyState != 4)
          {
            return
          }
          //if the request was successfull
          if (this.status === 200)
          {
            //get section number and article number
            var i = url_index_lookup[this.responseURL][0]
            var j = url_index_lookup[this.responseURL][1]
            //add html from response text to articles variable
            articles[i][j].push(this.responseText)
            //call function to display articles (since there is no easy way to check if this is the last article to load this is called every time)
            displayArticleThumbnails(0)
          }
          // if the request was unsuccessful print error to console
          else
          {
            console.error("error fetching: " + this.responseURL)
            ERRORS += "error fetching: " + this.responseURL
          }
        }
        nreq.send(null)//send request for article content
      }
    }
    // if the request was unsuccessful print error to console
    else
    {
      console.error("error fetching: " + this.responseURL)
      ERRORS += "error fetching: " + this.responseURL
    }
  }
  req.send(null)//send request for info file
}
