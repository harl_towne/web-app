// Library Imports
const express = require("express") //express use to manage web trafic
const cors = require('cors') //cors used to fix a bug with some browsers
const { Client } = require('pg') //pg used to access database
require('dotenv').config() //loads enviroment variables for running website localy

//create express object
var app = express()
//contect to db.
console.log("database is: " + process.env.DATABASE_URL)
const client = new Client({
  connectionString: process.env.DATABASE_URL
})
client.connect()
//setup push notifications


//set port number (8000 unless otherwise set)
var PORT = process.env.PORT
if (PORT == null || PORT == "") 
{
    PORT = 8000
}

//setup hogan templating
app.set("view engine", "html")
app.set('views', __dirname)
app.engine('html', require('hogan-express'))
//domain that the app is running on (set first time a get request is sent)
var __domain = ""

// the fixes an issue that prevents the website from loading on some browsers. Error: "NetworkError: Failed to execute 'send' on 'XMLHttpRequest"
// to my understanding some browsers won't accept a response unless this is done
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
  next();
})

//parse all requests marked as json
app.use("/json", express.json())

app
  //handling for user account creation
  .post("/json/register",  function register(req, res) {
    var data = req.body
    if(!data.hasOwnProperty('username') || !data.hasOwnProperty('password') || !data.hasOwnProperty('urn') || !data.hasOwnProperty('email') || !data.hasOwnProperty('phonenumber'))
    {
      console.log("bad registration request received: " + JSON.stringify(req.body))
      res.sendStatus(400) //bad request
    }
    console.log("received user registration for: " + data['username'])
    //register user in db
    const DBreq = client.query(
      //query to be sent:
      `INSERT INTO users (urn, name, email, phonenumber, password, checklist, subscription) VALUES ('${data['urn']}', '${data['username']}', '${data['email']}', '${data['phonenumber']}', '${data['password']}', '{}', '{}');`)
      //what to do if the query is succesful:
      .then(() =>
      {
        console.log("success")
        res.json(data)//send back username and password if successfull 
      })
      //what to do if the query fails
      .catch((reason)=>{
        console.error("ERROR: " + reason.detail)
        res.status(409).send("409 (Conflict): " + reason.detail)
      })
  })
  //handling for user login
  .post("/json/login", function login(req, res){
    var data = req.body
    if(!data.hasOwnProperty('urn') || !data.hasOwnProperty('password'))
    {
      console.log("bad login request received: " + JSON.stringify(req.body))
      res.sendStatus(400) //bad request
    }
    console.log("received user login for: " + data['urn'])
    //check login with db
    const DBreq = client.query(
      //query for getting all users with a matching urn and password
      `SELECT * FROM users WHERE urn = '${data['urn']}' AND password = '${data['password']}';`
    )
    //what to do if the querry succeeds
    .then((response)=>{
      //if there are no matching users send back and error message
      if(response.rowCount == 0)
      { 
        console.log("invalid user")
        res.sendStatus(409)
      }
      //if there is one matching user the send back success to log the user in
      else if(response.rowCount == 1)
      {
        console.log("successful login")
        res.json(data) //send back phonenumber and password if successfull
      }
      //if there are more than one matching user send back and error (this should never happen)
      else
      {
        console.warn("ERROR: Multiple identical users.")
        console.warn(response.rows)
        res.sendStatus(500)
      }
    })
    //what to do if the query fails
    .catch((reason)=>{
      //send back and error
      console.error(reason)
      res.sendStatus(500)
    })
  })
  //handling for retreiving checklist
  .post("/json/getchecklist", function getChecklist(req, res){
    var data = req.body
    if(!data.hasOwnProperty('urn') || !data.hasOwnProperty('password'))
    {
      console.log("bad login request received: " + JSON.stringify(req.body))
      res.sendStatus(400) //bad request
    }
    //check login with db
    const DBreq = client.query(
      //query for getting all users with a matching urn and password
      `SELECT * FROM users WHERE urn = '${data['urn']}' AND password = '${data['password']}';`
    )
    //what to do if the querry succeeds
    .then((response)=>{
      //if there are no matching users send back and error message
      if(response.rowCount == 0)
      { 
        console.log("invalid user")
        res.sendStatus(409)
      }
      //if there is one matching user the send back success to log the user in
      else if(response.rowCount == 1)
      {
        console.log("successful login")
        //start new query to get checklist data
        const DBreq2 = client.query(
          `SELECT checklist FROM users WHERE urn = '${data['urn']}' AND password = '${data['password']}';`
        )
        //what to do if the querry succeeds
        .then((response) =>
        {
          console.log("returned checklist for: " + data['urn'])
          res.json(response["rows"][0]["checklist"]) //return checklist for user
        })
        //what to do if the query fails
        .catch((reason) =>
        {
          console.error("failed to return checklist for: " + data['urn'])
          console.error(reason)
          res.sendStatus(500) //respond with 500 (internal server error)
        })
      }
      //if there are more than one matching user send back and error (this should never happen)
      else
      {
        console.warn("ERROR: Multiple identical users.")
        console.warn(response.rows)
        res.sendStatus(500) //respond with 500 (internal server error)
      }
    })
    //what to do if the query fails
    .catch((reason)=>{
      //send back and error
      console.error(reason)
      res.sendStatus(500)
    })    
  })
  //handling for retreiving checklist
  .post("/json/setchecklist", function getChecklist(req, res){
    var data = req.body
    if(!data.hasOwnProperty('urn') || !data.hasOwnProperty('password')|| !data.hasOwnProperty('checklist'))
    {
      console.log("bad request received: " + JSON.stringify(req.body))
      res.sendStatus(400) //bad request
    }
    //check login with db
    const DBreq = client.query(
      //query for getting all users with a matching urn and password
      `SELECT * FROM users WHERE urn = '${data['urn']}' AND password = '${data['password']}';`
    )
    //what to do if the querry succedes
    .then((response)=>{
      //if there are no matching users send back and error message
      if(response.rowCount == 0)
      { 
        console.log("invalid user")
        res.sendStatus(409)
      }
      //if there is one matching user the send back success to log the user in
      else if(response.rowCount == 1)
      {
        console.log("successful login")
        //start new querry to update checklist
        const DBreq2 = client.query(
          `UPDATE users SET checklist = '${JSON.stringify(data["checklist"])}' WHERE urn = '${data['urn']}' AND password = '${data['password']}';`
        )
        //what to do if the querry succedes
        .then((response) =>
        {
          console.log("updated checklist for: " + data['urn'])
          res.sendStatus(200) //send OK status
        })
        //what to do if the query fails
        .catch((reason) =>
        {
          console.error("failed to update checklist for: " + data['urn'])
          console.error(reason)
          res.sendStatus(500) //respond with 500 (internal server error)
        })
      }
      //if there are more than one matching user send back and error (this should never happen)
      else
      {
        console.warn("ERROR: Multiple identical users.")
        console.warn(response.rows)
        res.sendStatus(500) //respond with 500 (internal server error)
      }
    })
    //what to do if the query fails
    .catch((reason)=>{
      //send back and error
      console.error(reason)
      res.sendStatus(500)
    })    
  })
  //handling for subscribing to notifications
  .post('/json/subscribe', (req, res) => {
    var data = req.body
    if(!data.hasOwnProperty('urn') || !data.hasOwnProperty('password')|| !data.hasOwnProperty('subscription'))
    {
      console.log("bad request received: " + JSON.stringify(req.body))
      res.sendStatus(400) //bad request
    }
    //check login with db
    const DBreq = client.query(
      //query for getting all users with a matching urn and password
      `SELECT * FROM users WHERE urn = '${data['urn']}' AND password = '${data['password']}';`
    )
    //what to do if the querry succedes
    .then((response)=>{
      //if there are no matching users send back and error message
      if(response.rowCount == 0)
      { 
        console.log("invalid user")
        res.sendStatus(409)
      }
      //if there is one matching user the send back success to log the user in
      else if(response.rowCount == 1)
      {
        console.log("successful login")
        //start new querry to update subscription
        const DBreq2 = client.query(
          `UPDATE users SET subscription = '${JSON.stringify(data["subscription"])}' WHERE urn = '${data['urn']}' AND password = '${data['password']}';`
        )
        //what to do if the querry succedes
        .then((response) =>
        {
          console.log("updated subscription for: " + data['urn'])
          res.sendStatus(200) //send OK status
        })
        //what to do if the query fails
        .catch((reason) =>
        {
          console.error("failed to update subscription for: " + data['urn'])
          console.error(reason)
          res.sendStatus(500) //respond with 500 (internal server error)
        })
      }
      //if there are more than one matching user send back and error (this should never happen)
      else
      {
        console.warn("ERROR: Multiple identical users.")
        console.warn(response.rows)
        res.sendStatus(500) //respond with 500 (internal server error)
      }
    })
    //what to do if the query fails
    .catch((reason)=>{
      //send back and error
      console.error(reason)
      res.sendStatus(500)
    })
  })
  //for when the index page is requested
  .get("/", function returnIndex(req, res){
  //if __domain isn't set the set it
  if(__domain == "" || __domain == null)
  {
    if(req.hostname == "localhost")
    {
      __domain = "http://localhost:5000"
    }
    else
    {
      __domain = "https://" + req.hostname
    }
    console.log("__domain = " + __domain)
  }
  //check if the request is http instead of https and if the domain is not localhost
  if (__domain != "http://localhost:5000" && req.headers['x-forwarded-proto'] !== 'https')
  {
    //if so redirect the user
    res.redirect("https://" + req.hostname + req.path)
  }
  //if the user is requesting the home webpage then send index.html. render is used to set all links in index.html to point to whatever url this code is running
  else if(req.path == "/")
  {
    res.render("./public/index.html", {domain:__domain})
  }
})

//for all unhandeld request try to get an apropriate resource from the public folder
app.use("/", express.static("public/"))

//start app and listen on PORT
app.listen(PORT)
console.log("Web app listening on PORT: " + PORT)
